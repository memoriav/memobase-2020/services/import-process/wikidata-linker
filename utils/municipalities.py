#!/bin/env python3

mun_de = {}
mun_fr = {}
mun_it = {}
cantons = {
'ag': 11904,
'ar': 11918,
'ai': 11920,
'bl': 11930,
'bs': 11929,
'be': 11931,
'fr': 11935,
'ge': 11946,
'gl': 11949,
'gr': 11950,
'ju': 11959,
'lu': 11960,
'ne': 11967,
'nw': 11969,
'ow': 11972,
'sh': 11989,
'sz': 11991,
'so': 11994,
'sg': 11981,
'tg': 12003,
'ti': 12004,
'ur': 12011,
'vs': 12007,
'vd': 12010,
'zg': 12295,
'zh': 11943
}
qids = set()

  #country_qid: 39
  # continent_qid: 46

with open('configs/wd_mappings/mun_de.yaml', encoding='utf-8') as f:
    for l in f:
        r = l.split(',')
        qid = r[2].replace('http://www.wikidata.org/entity/Q', '').strip()
        label = r[0].lower()
        plate = cantons[r[1].lower()]
        mun_de[qid] = (label, plate)
        qids.add(qid)

with open('configs/wd_mappings/mun_fr.yaml', encoding='utf-8') as f:
    for l in f:
        r = l.split(',')
        qid = r[2].replace('http://www.wikidata.org/entity/Q', '').strip()
        label = r[0].lower()
        plate = cantons[r[1].lower()]
        mun_fr[qid] = (label, plate)
        qids.add(qid)

with open('configs/wd_mappings/mun_it.yaml', encoding='utf-8') as f:
    for l in f:
        r = l.split(',')
        qid = r[2].replace('http://www.wikidata.org/entity/Q', '').strip()
        label = r[0].lower()
        plate = cantons[r[1].lower()]
        mun_it[qid] = (label, plate)
        qids.add(qid)

l = []
for qid in qids:
    if qid in mun_de:
        label_de = mun_de[qid][0]
    if qid in mun_fr:
        label_fr = mun_fr[qid][0]
    if qid in mun_it:
        label_it = mun_it[qid][0]
    if label_de == label_fr and label_de == label_it:
        l.append((label_de, qid, mun_de[qid][1], "['de', 'fr', 'it']"))
    elif label_de == label_fr:
        l.append((label_de, qid, mun_de[qid][1], "['de', 'fr']"))
        l.append((label_it, qid, mun_de[qid][1], "['it']"))
    elif label_de == label_it:
        l.append((label_de, qid, mun_de[qid][1], "['de', 'it']"))
        l.append((label_fr, qid, mun_de[qid][1], "['fr']"))
    elif label_fr == label_it:
        l.append((label_fr, qid, mun_de[qid][1], "['fr', 'it']"))
        l.append((label_de, qid, mun_de[qid][1], "['de']"))
    else:
        l.append((label_de, qid, mun_de[qid][1], "['de']"))
        l.append((label_fr, qid, mun_de[qid][1], "['fr']"))
        l.append((label_it, qid, mun_de[qid][1], "['it']"))
for e in l:
    print('- name: ' + e[0])
    print('  lang: ' + e[3])
    print('  qid: ' + e[1])
    print('  state_qid: ' + str(e[2]))
    print('  country_qid: 39')
    print('  continent_qid: 46')
