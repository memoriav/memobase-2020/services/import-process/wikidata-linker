# Wikidata Linker

Extracts and links named entities in structured Memobase metadata fields

The linker parses structured metadata fields based on a predefined extractor. If
an extractor can be applied on a value, the resulting entities (e.g. locations)
are checked against a list of Wikidata entities and, if a similar entity is
found, enriched with the link to the Wikidata entity. Extractors are defined
per collection, and one collection can have multiple extractors. The extractors
are applied in the order they are defined in the configuration.

## Development Requirements

[Rust](https://www.rust-lang.org/tools/install)

## Running

### API

#### `/link` endpoint

Request (example)

```sh
curl -XPOST https://localhost:3000/link --data '{"value":"Glarus-Süd, GL (Schweiz), Europa","value_type":"location","collection":"bla-001","verbose":true}' -H 'Content-Type: application/json' --cacert certs/ca.crt --cert certs/tls.crt --key certs/tls.key
```

Request data:

```json
{
    "value": "The value of the structured metadata field",
    "value_type": "location or agent",
    "collection": "collection id",
    "verbose": true
}
```

Response

```json
{
        "value": "original value of the structured metadata field",
        "matches": [
            {
                "value": "found entity value",
                "qid": 1232,
                "match_type": "mun, sta, staa, cou, coua, con, cona, per or org", 
                "range": {
                    "start": 0, 
                    "end": 6
                },
                "lang": ["de"]
            }
        ],
        "extractor": "name of extractor"
}
```

#### `/health` endpoint

Request

```sh
curl -XGET localhost:3000/health --cacert ca_cert.pem --cert client_cert.pem --key client_key.pem
```

Response

`OK`

## Configuration

The application expects the following configurations:

- The application configuration;
- The extractor configuration;
- The Wikidata configuration (these are included, but can be overridden)

### Application configuration

### Extractor configuration

This file contains a map of collections with a list of one or more attributed extractors.

```yaml
collection_name:
- name: pattern_name
  args: ["optional", "arguments"]
another_collection_name:
- name: pattern_name
  args: ["optional", "arguments"]
```

Normally, a `collection_name` is identical to the collection identifier. The collection name `*` serves as a fallback for all collections which have no explicitly defined extractors.

An extractor consists of a name and depending on the extractor a list of additional arguments.

At the moment, these extractors are supported:

- `pattern`: An extractor based on a regular expression
- `noop`: A dummy extractor

#### `pattern` extractor

The pattern extractor takes a regular expression and tries to match this
expression against a value. In order to provide the application with the needed
semantic, you have to define named capture groups with the following values to
extract respective entities:

| name | description |
| ---  | --- |
| `mun` | A municipality |
| `sta` | A state (canton), not abbreviated |
| `staa` | A state code |
| `cou` | A country |
| `coua` | Country code |
| `con` | Continent |
| `cona` | Abbreviated continent |
| `per` | Person |
| `org` | Organisation |

An example regex:

```regex
^(?<mun>[-\p{Latin}\. /]+), (?<staa>[A-Z]{2}) \((?<cou>[-\p{Latin}]+)\), (?<con>[\p{Latin} ]+)$
```

#### `noop` extractor

An extractor which does nothing. For mere testing purposes.

### Wikidata configuration

#### `mun.yaml`

A list of municipalities.

```yaml
- name: le glèbe                # Lowercase letters are important!
  lang: ['de', 'fr', 'it']      # Currently supported languages: de, fr, it
  qid: 66436                    # Wikidata municipality entity id (without the Q prefix)
  state_qid: 11935              # Wikidata state entity id (without the Q prefix) 
  country_qid: 39               # Wikidata country entity id (without the Q prefix) 
  continent_qid: 46             # Wikidata continent entity id (without the Q prefix) 
- name: orges                   # Another entry...
  ...
```

#### `sta.yaml` and `staa.yaml`

A list of states (`sta.yaml`) and state codes (`staa.yaml`)

```yaml
- name: ag                      # Lowercase letters are important!
  lang: ['de', 'fr', 'it']      # Currently supported languages: de, fr, it
  qid: 11904                    # Wikidata state entity id (without the Q prefix) 
  country_qid: 39               # Wikidata country entity id (without the Q prefix) 
  continent_qid: 46             # Wikidata continent entity id (without the Q prefix) 
```

#### `cou.yaml` and `coua.yaml`

A list of countries (`cou.yaml`) and country codes (`coua.yaml`)

```yaml
- name: costa rica              # Lowercase letters are important!
  lang: ['it']                  # Currently supported languages: de, fr, it
  qid: 800                      # Wikidata country entity id (without the Q prefix) 
  continent_qid: 49             # Wikidata continent entity id (without the Q prefix) 
```

#### `con.yaml` and `cona.yaml`

A list of continents (`con.yaml`) and abbreviated continents (`cona.yaml`)

```yaml
- name: europe                  # Lowercase letters are important!
  lang: [fr]                    # Currently supported languages: de, fr, it
  qid: 46                       # Wikidata continent entity id (without the Q prefix) 
```
