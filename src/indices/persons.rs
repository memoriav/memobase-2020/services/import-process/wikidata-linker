//! Contains the utils to index person entities from Wikidata

use std::collections::{HashMap, HashSet};

use serde::Deserialize;

use crate::api::models::LinkedEntity;
use crate::extraction::{FoundEntity, MatchType};
use crate::indices::Lang;

/// Localised name
#[derive(Clone, Debug, Hash, PartialEq, Eq, Deserialize)]
pub struct LocalisedName {
    /// Name value
    name: String,
    /// Related languages
    lang: Vec<String>,
}

/// Wikidata person entity
#[derive(Clone, Debug, Deserialize)]
pub struct Person {
    /// Full name
    #[serde(skip_serializing_if = "Option::is_none")]
    labels: Option<Vec<LocalisedName>>,
    /// Persons's Wikidata ID (without the Q prefix)
    qid: usize,
    /// First name
    #[serde(skip_serializing_if = "Option::is_none")]
    given_names: Option<Vec<LocalisedName>>,
    /// Last name
    #[serde(skip_serializing_if = "Option::is_none")]
    family_names: Option<Vec<LocalisedName>>,
}

/// Contains all indices to look up Wikidata person entities
#[derive(Clone, Debug, PartialEq, Eq)]
pub struct PersonsIdx {
    /// Maps given names to persons in Wikidata
    given_name: HashMap<String, HashSet<usize>>,
    /// Maps family names to persons in Wikidata
    family_name: HashMap<String, HashSet<usize>>,
    /// Maps name labels to persons in Wikidata
    name: HashMap<String, HashSet<usize>>,
}

impl PersonsIdx {
    /// Creates person indices
    pub fn new(persons: Vec<Person>) -> Self {
        let mut given_name_index = HashMap::with_capacity(persons.len());
        let mut family_name_index = HashMap::with_capacity(persons.len());
        let mut name_index = HashMap::with_capacity(persons.len());
        for person in persons {
            if let Some(labels) = person.labels {
                for label in labels {
                    name_index
                        .entry(label.name)
                        .and_modify(|entries: &mut HashSet<usize>| {
                            entries.insert(person.qid);
                        })
                        .or_insert_with(|| {
                            let mut hs = HashSet::new();
                            hs.insert(person.qid);
                            hs
                        });
                }
            }
            if let Some(given_names) = person.given_names {
                for given_name in given_names {
                    given_name_index
                        .entry(given_name.name)
                        .and_modify(|entries: &mut HashSet<usize>| {
                            entries.insert(person.qid);
                        })
                        .or_insert_with(|| {
                            let mut hs = HashSet::new();
                            hs.insert(person.qid);
                            hs
                        });
                }
            }
            if let Some(family_names) = person.family_names {
                for family_name in family_names {
                    family_name_index
                        .entry(family_name.name)
                        .and_modify(|entries: &mut HashSet<usize>| {
                            entries.insert(person.qid);
                        })
                        .or_insert_with(|| {
                            let mut hs = HashSet::new();
                            hs.insert(person.qid);
                            hs
                        });
                }
            }
        }
        Self {
            given_name: given_name_index,
            family_name: family_name_index,
            name: name_index,
        }
    }

    /// Searches Wikidata person entities with matching full name
    pub fn lookup_name(&self, entity: &FoundEntity) -> Option<LinkedEntity> {
        if let Some(ids) = self.name.get(&entity.value.to_lowercase()) {
            // TODO: At the moment, a match must be unambiguously. Find a more fine-grained
            // solution in the future
            if ids.len() == 1 {
                return Some(LinkedEntity {
                    value: entity.value.to_string(),
                    qid: *ids.iter().next().unwrap(),
                    match_type: entity.match_type,
                    range: entity.range.clone(),
                    lang: vec![Lang::De, Lang::Fr, Lang::It],
                });
            }
        }
        None
    }

    /// Searches Wikidata person entities with matching given and family name
    pub fn lookup_given_family_name(
        &self,
        given_name: &FoundEntity,
        family_name: &FoundEntity,
    ) -> Option<LinkedEntity> {
        let given_name_ids = self.given_name.get(&given_name.value.to_lowercase());
        /*.map(|ids| {
            ids.iter()
                .filter_map(|id| self.table.get(id).map(|qid| (id, qid)))
                .map(|ids| (*ids.0, *ids.1))
                .collect::<Vec<(u64, usize)>>()
        });*/
        let family_name_ids = self.family_name.get(&family_name.value.to_lowercase());
        /*.map(|ids| {
            ids.iter()
                .filter_map(|id| self.table.get(id).map(|qid| (id, qid)))
                .map(|ids| (*ids.0, *ids.1))
                .collect::<Vec<(u64, usize)>>()
        });*/
        match (given_name_ids, family_name_ids) {
            (Some(gn_ids), Some(fn_ids)) => {
                let mut shared_ids = vec![];
                for gn_id in gn_ids {
                    for fn_id in fn_ids {
                        if gn_id == fn_id {
                            shared_ids.push(*gn_id);
                        }
                    }
                }
                // TODO: At the moment, a match must be unambiguously. Find a more fine-grained
                // solution in the future
                if shared_ids.len() == 1 {
                    let qid = shared_ids.first().unwrap();
                    let shared_range = if given_name.range.start < family_name.range.start {
                        given_name.range.start
                    } else {
                        family_name.range.start
                    }
                        ..if given_name.range.end > family_name.range.end {
                            given_name.range.end
                        } else {
                            family_name.range.end
                        };
                    Some(LinkedEntity {
                        value: format!("{} {}", given_name.value, family_name.value),
                        qid: *qid,
                        // TODO: Find better match type
                        match_type: MatchType::PersonFullName,
                        range: shared_range,
                        lang: vec![Lang::De, Lang::Fr, Lang::It],
                    })
                } else {
                    None
                }
            }
            _ => None,
        }
    }
}
