//! Contains general utils for building and querying Wikidata entity indices

pub mod locations;
pub mod persons;

use serde::{Deserialize, Serialize};
use typed_builder::TypedBuilder;

use crate::{
    api::models::LinkedEntity,
    extraction::{FoundEntity, MatchType},
};

use crate::indices::locations::LocationContext;
use locations::Locations;
use persons::PersonsIdx;

/// Indices of Wikidata entities
#[derive(TypedBuilder)]
pub struct Indices {
    /// Municipality index
    mun: Locations,
    /// State index
    sta: Locations,
    /// State code index
    staa: Locations,
    /// Country index
    cou: Locations,
    /// Country code index
    coua: Locations,
    /// Continent index
    con: Locations,
    /// Continent abbrevation index
    cona: Locations,
    /// Person index
    per: PersonsIdx,
}

impl Indices {
    /// Searches location indices
    pub fn location_lookup(
        &self,
        mt: MatchType,
        entity: &FoundEntity,
        state_qid: &LocationContext,
        country_qid: &LocationContext,
        continent_qid: &LocationContext,
    ) -> Option<LinkedEntity> {
        match mt {
            MatchType::Municipality => {
                self.mun
                    .lookup(entity, state_qid, country_qid, continent_qid)
            }
            MatchType::State => self.sta.lookup(
                entity,
                &LocationContext::NotAvailable,
                country_qid,
                continent_qid,
            ),
            MatchType::StateAbbr => self.staa.lookup(
                entity,
                &LocationContext::NotAvailable,
                country_qid,
                continent_qid,
            ),
            MatchType::Country => self.cou.lookup(
                entity,
                &LocationContext::NotAvailable,
                &LocationContext::NotAvailable,
                continent_qid,
            ),
            MatchType::CountryAbbr => self.coua.lookup(
                entity,
                &LocationContext::NotAvailable,
                &LocationContext::NotAvailable,
                continent_qid,
            ),
            MatchType::Continent => self.con.lookup(
                entity,
                &LocationContext::NotAvailable,
                &LocationContext::NotAvailable,
                &LocationContext::NotAvailable,
            ),
            MatchType::ContinentAbbr => self.cona.lookup(
                entity,
                &LocationContext::NotAvailable,
                &LocationContext::NotAvailable,
                &LocationContext::NotAvailable,
            ),
            _ => None,
        }
    }

    /// Searches in person's full name index
    pub fn person_full_name_lookup(&self, entity: &FoundEntity) -> Option<LinkedEntity> {
        self.per.lookup_name(entity)
    }

    /// Searches in person's given and family name indices
    pub fn person_given_family_name_lookup(
        &self,
        given_name: &FoundEntity,
        family_name: &FoundEntity,
    ) -> Option<LinkedEntity> {
        self.per.lookup_given_family_name(given_name, family_name)
    }
}

/// Supported Wikidata label languages
#[derive(Clone, Debug, Hash, PartialEq, Eq, Deserialize, Serialize)]
#[serde(rename_all = "lowercase")]
pub enum Lang {
    /// German
    De,
    /// French
    Fr,
    /// Italian
    It,
}
