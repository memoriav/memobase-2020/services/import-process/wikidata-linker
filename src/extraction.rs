//! Contains utils for extracting entity matches

use std::{fmt::Display, ops::Range};

use anyhow::{bail, Context, Result};
use regex::{Match, Regex};
use serde::{Deserialize, Serialize};
use serde_variant::to_variant_name;
use tracing::{debug, info, warn};

use crate::config::ExtractorConfig;

/// An identified entity in a value
#[derive(Clone, Debug, Serialize, PartialEq, Eq)]
pub struct FoundEntity<'a> {
    /// String value
    pub value: &'a str,
    /// Type of match
    pub match_type: MatchType,
    /// Range in overall value (haystack)
    pub range: Range<usize>,
}

impl FoundEntity<'_> {
    /// Creates a [`FoundEntity`] instance
    pub fn build(m: Match, match_type: MatchType) -> FoundEntity {
        let range = m.range();
        let value = m.as_str();
        FoundEntity {
            value,
            match_type,
            range,
        }
    }
}

impl Display for FoundEntity<'_> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let json = serde_json::to_string_pretty(self).map_err(|_| std::fmt::Error)?;
        writeln!(f, "{json}")
    }
}

impl PartialOrd for FoundEntity<'_> {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        Some(self.cmp(other))
    }
}

impl Ord for FoundEntity<'_> {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        self.match_type.cmp(&other.match_type)
    }
}

/// Supported match categories
#[derive(Clone, Copy, Debug, Deserialize, Serialize, PartialEq, PartialOrd, Ord, Eq, Hash)]
pub enum MatchType {
    /// A municipality, not abbreviated
    #[serde(rename = "mun")]
    Municipality = 7,
    /// A state, not abbreviated
    #[serde(rename = "sta")]
    State = 5,
    /// A state code
    #[serde(rename = "staa")]
    StateAbbr = 6,
    /// A country
    #[serde(rename = "cou")]
    Country = 3,
    /// Country code
    #[serde(rename = "coua")]
    CountryAbbr = 4,
    /// A continent
    #[serde(rename = "con")]
    Continent = 1,
    /// Continent code
    #[serde(rename = "cona")]
    ContinentAbbr = 2,
    /// A person's full name
    #[serde(rename = "per")]
    PersonFullName = 8,
    /// A person's given name
    #[serde(rename = "pgn")]
    PersonGivenName = 9,
    /// A person's family name
    #[serde(rename = "pfn")]
    PersonFamilyName = 10,
    /// An organisation
    #[serde(rename = "org")]
    Organisation = 11,
}

impl Display for MatchType {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let name = match self {
            Self::Municipality => "municipality",
            Self::State => "state",
            Self::StateAbbr => "state code",
            Self::Country => "country",
            Self::CountryAbbr => "country code",
            Self::Continent => "continent",
            Self::ContinentAbbr => "continent code",
            Self::PersonFullName => "full name of person",
            Self::PersonGivenName => "given name of person",
            Self::PersonFamilyName => "family name of person ",
            Self::Organisation => "organisation",
        };
        writeln!(f, "{name}")
    }
}

/// Tries defined extractors in the defined order. Returns a list of found entities if entities are
/// successfully extracted
pub fn extract<'a>(
    value: &'a str,
    extractors: &'a [Extractor],
) -> Vec<(&'a Extractor, Vec<FoundEntity<'a>>)> {
    extractors
        .iter()
        .filter_map(|extr| match extr.extract(value) {
            Ok(Some(m)) => {
                debug!("Match with extractor {} found", extr.name());
                Some((extr, m))
            }
            Ok(None) => {
                debug!(
                    "No match with extractor {}. Trying next extractor",
                    extr.name()
                );
                None
            }
            Err(e) => {
                warn!("{}", e);
                None
            }
        })
        .collect()
}

/// Supported extractor categories
pub enum Extractor {
    /// A pattern extractor
    Pattern(PatternExtractor),
    /// A noop (no-operation) extractor
    Noop(NoOpExtractor),
}

impl Extractor {
    /// Shows name of extractor
    pub fn name(&self) -> String {
        match self {
            Self::Pattern(ext) => ext.name(),
            Self::Noop(_) => "noop".to_string(),
        }
    }

    /// Tries to extract matches
    pub fn extract<'a>(&self, value: &'a str) -> Result<Option<Vec<FoundEntity<'a>>>> {
        match self {
            Self::Pattern(ext) => ext.extract(value),
            Self::Noop(_) => Ok(None),
        }
    }
}

impl TryFrom<&ExtractorConfig> for Extractor {
    type Error = anyhow::Error;

    fn try_from(value: &ExtractorConfig) -> std::prelude::v1::Result<Self, Self::Error> {
        match value.name.as_str() {
            "pattern" => {
                let pattern = value.args.as_ref().map_or_else(
                    || {
                        info!("Pattern extractor with no pattern: Set to match all pattern (.*)");
                        ".*".to_string()
                    },
                    std::clone::Clone::clone,
                );
                let pattern_extractor = PatternExtractor::build(&pattern)?;
                Ok(Self::Pattern(pattern_extractor))
            }

            "noop" => Ok(Self::Noop(NoOpExtractor)),
            extr => bail!("Unknown extractor name '{}'", extr),
        }
    }
}

/// Does nothing
pub struct NoOpExtractor;

/// Extracts entities based on a regex pattern
pub struct PatternExtractor {
    /// Regex pattern
    pattern: Regex,
}

impl PatternExtractor {
    /// Build a new pattern extractor instance
    pub fn build(pattern: &str) -> Result<Self> {
        let pattern: Regex = Regex::new(pattern).context("Parsing regex failed")?;
        Ok(Self { pattern })
    }
}

impl PatternExtractor {
    /// Tries to find matches in haystack based on pattern
    fn extract<'a>(&self, value: &'a str) -> Result<Option<Vec<FoundEntity<'a>>>> {
        #[allow(clippy::enum_glob_use)]
        use MatchType::*;
        if let Some(caps) = self.pattern.captures(value) {
            let matches = vec![
                (caps.name(to_variant_name(&Municipality)?), Municipality),
                (caps.name(to_variant_name(&State)?), State),
                (caps.name(to_variant_name(&StateAbbr)?), StateAbbr),
                (caps.name(to_variant_name(&Country)?), Country),
                (caps.name(to_variant_name(&CountryAbbr)?), CountryAbbr),
                (caps.name(to_variant_name(&Continent)?), Continent),
                (caps.name(to_variant_name(&ContinentAbbr)?), ContinentAbbr),
                (caps.name(to_variant_name(&PersonFullName)?), PersonFullName),
                (
                    caps.name(to_variant_name(&PersonGivenName)?),
                    PersonGivenName,
                ),
                (
                    caps.name(to_variant_name(&PersonFamilyName)?),
                    PersonFamilyName,
                ),
            ];
            let named_entities = matches
                .into_iter()
                .filter_map(|cap| cap.0.map(|m| FoundEntity::build(m, cap.1)))
                .collect::<Vec<FoundEntity>>();
            if named_entities.is_empty() {
                Ok(None)
            } else {
                Ok(Some(named_entities))
            }
        } else {
            debug!("Haystack doesn't match");
            Ok(None)
        }
    }

    /// Returns name of pattern extractor
    fn name(&self) -> String {
        format!("pattern({})", self.pattern.as_str())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_no_op() {
        let no_op_extractor = Extractor::Noop(NoOpExtractor);
        let extractors = &[no_op_extractor];
        let res = extract("test_value", extractors);
        assert!(res.is_empty());
    }

    #[test]
    fn test_loc_pattern_extractor() {
        let haystack = "Glarus-Süd, GL (Schweiz), Europe occidentale";
        let pattern_extractor = PatternExtractor::build(
            r"^(?<mun>[-\p{Latin}\. /]+), (?<staa>[A-Z]{2}) \((?<cou>[-\p{Latin}]+)\), (?<con>[\p{Latin} ]+)$",
        )
            .unwrap();
        let extractors = [
            Extractor::Noop(NoOpExtractor),
            Extractor::Pattern(pattern_extractor),
        ];
        if let Some((_, res)) = extract(haystack, &extractors).first() {
            let mut res = res.clone();
            res.sort();
            let m = res.first().unwrap();
            assert!(m.match_type == MatchType::Continent);
            assert!(m.value == "Europe occidentale");
            let m = res.get(1).unwrap();
            assert!(m.match_type == MatchType::Country);
            assert!(m.value == "Schweiz");
            let m = res.get(2).unwrap();
            assert!(m.match_type == MatchType::StateAbbr);
            assert!(m.value == "GL");
            let m = res.get(3).unwrap();
            assert!(m.match_type == MatchType::Municipality);
            assert!(m.value == "Glarus-Süd");
        } else {
            panic!();
        }
    }

    #[test]
    fn test_loc_pattern_extractors() {
        let haystack = "Glarus-Süd";
        let pattern_extractor_1 = PatternExtractor::build(
            r"^(?<mun>[-\p{Latin}\. /]+), (?<staa>[A-Z]{2}) \((?<cou>[-\p{Latin}]+)\), (?<con>[\p{Latin} ]+)$",
        )
            .unwrap();
        let pattern_extractor_2 = PatternExtractor::build(r"^(?<mun>.*)$").unwrap();
        let extractors = [
            Extractor::Pattern(pattern_extractor_1),
            Extractor::Pattern(pattern_extractor_2),
        ];
        if let Some((_, res)) = extract(haystack, &extractors).first() {
            let mut res = res.clone();
            res.sort();
            let m = res.first().unwrap();
            assert!(m.match_type == MatchType::Municipality);
            assert!(m.value == "Glarus-Süd");
        } else {
            panic!();
        }
    }

    #[test]
    fn test_person_pattern_extraction() {
        let haystack = "Max Frisch";
        let given_family_name_extractor =
            PatternExtractor::build(r"^(?<pgn>[-\p{Latin}\.']+) (?<pfn>[-\p{Latin}\.']+)$")
                .unwrap();
        let full_name_extractor = PatternExtractor::build(r"^(?<per>.+)$").unwrap();
        let extractors = [
            Extractor::Pattern(given_family_name_extractor),
            Extractor::Pattern(full_name_extractor),
        ];
        let extracted_entities = extract(haystack, &extractors);
        if let Some((_, res)) = extracted_entities.first() {
            let m = res.first().unwrap();
            assert!(m.match_type == MatchType::PersonGivenName);
            assert!(m.value == "Max");
            let m = res.get(1).unwrap();
            assert!(m.match_type == MatchType::PersonFamilyName);
            assert!(m.value == "Frisch");
        } else {
            panic!();
        }
        if let Some((_, res)) = extracted_entities.get(1) {
            let m = res.first().unwrap();
            assert!(m.match_type == MatchType::PersonFullName);
            assert!(m.value == "Max Frisch");
        } else {
            panic!();
        }
    }

    #[test]
    fn match_type_ordering() {
        let mut match_types = vec![
            MatchType::Country,
            MatchType::Continent,
            MatchType::Municipality,
            MatchType::State,
        ];
        match_types.sort();
        assert_eq!(
            match_types,
            vec![
                MatchType::Continent,
                MatchType::Country,
                MatchType::State,
                MatchType::Municipality,
            ]
        );
    }

    #[test]
    fn found_entity_ordering() {
        let lesser = FoundEntity {
            value: "blaba",
            match_type: MatchType::Continent,
            range: 0..1,
        };
        let greater = FoundEntity {
            value: "waefw",
            match_type: MatchType::Municipality,
            range: 9..20,
        };
        let mut found_entities = vec![greater.clone(), lesser.clone()];
        found_entities.sort();
        assert_eq!(found_entities, vec![lesser, greater]);
    }
}
