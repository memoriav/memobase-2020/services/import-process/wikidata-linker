#![allow(clippy::doc_markdown)]
//! Endpoints offered by the RESTful API
//!
//! This module provides the endpoints which are exposed by the server's RestAPI

use axum::{
    extract::State,
    routing::{get, post},
    Json, Router,
};
use http::StatusCode;
use tracing::debug;

use crate::extraction::{self, FoundEntity, MatchType};
use crate::indices::locations::LocationContext;

use self::models::{AppError, AppState, LinkedEntity, Match, MatchRequest};

pub mod models {
    //! Models used by the API

    use std::{collections::HashMap, fmt::Display, ops::Range, sync::Arc};

    use crate::{
        config::{Config, ExtractorConfig},
        extraction::{Extractor, MatchType},
        indices::{Indices, Lang},
    };
    use serde::{Deserialize, Serialize};
    use tracing::warn;

    /// State needed by the API handlers
    #[derive(Clone)]
    pub struct AppState {
        /// Application configuration
        #[allow(dead_code)]
        pub config: Arc<Config>,
        /// Available extractors
        pub extractors: Arc<Extractors>,
        /// Wikipedia entity indices
        pub indices: Arc<Indices>,
    }

    /// Contains the mappings from collections to assigned extractors and possible fallback
    /// extractors
    pub struct Extractors {
        /// Mapping from collections to agent extractors
        agents: HashMap<String, Vec<Extractor>>,
        /// Fallback agent extractors
        agent_default: Vec<Extractor>,
        /// Mapping from collections to location extractors
        locations: HashMap<String, Vec<Extractor>>,
        /// Fallback location extractors
        location_default: Vec<Extractor>,
    }

    impl Extractors {
        /// Builds extractor tables from parsed extractor configurations
        pub fn build(
            agent_extractor_configs: HashMap<String, Vec<ExtractorConfig>>,
            location_extractor_configs: HashMap<String, Vec<ExtractorConfig>>,
        ) -> Self {
            let agent_default = agent_extractor_configs
                .get("*")
                .map_or_else(Vec::new, |fallback_extractor| {
                    Self::build_extractors_from_config(fallback_extractor)
                });
            let agents = agent_extractor_configs
                .into_iter()
                .filter(|e| e.0 != "*")
                .map(|e| (e.0, Self::build_extractors_from_config(&e.1)))
                .collect();
            let location_default = location_extractor_configs
                .get("*")
                .map_or_else(Vec::new, |fallback_extractor| {
                    Self::build_extractors_from_config(fallback_extractor)
                });
            let locations = location_extractor_configs
                .into_iter()
                .filter(|e| e.0 != "*")
                .map(|e| (e.0, Self::build_extractors_from_config(&e.1)))
                .collect();
            Self {
                agents,
                agent_default,
                locations,
                location_default,
            }
        }

        /// Returns agent extractors for collection
        pub fn get_agent_extractors(&self, collection_name: &str) -> &Vec<Extractor> {
            self.agents
                .get(collection_name)
                .unwrap_or(&self.agent_default)
        }

        /// Returns location extractors for collection
        pub fn get_location_extractors(&self, collection_name: &str) -> &Vec<Extractor> {
            self.locations
                .get(collection_name)
                .unwrap_or(&self.location_default)
        }

        /// Builds extractors from parsed extractor configurations
        fn build_extractors_from_config(extractor_configs: &[ExtractorConfig]) -> Vec<Extractor> {
            extractor_configs
                .iter()
                .map(Extractor::try_from)
                .filter_map(|e| match e {
                    Ok(r) => Some(r),
                    Err(err) => {
                        warn!("Extractor generation failed: {}", err);
                        None
                    }
                })
                .collect()
        }
    }

    /// A request for matching a value
    #[derive(Clone, Deserialize, Serialize)]
    pub struct MatchRequest {
        /// Original value in which matches should be found
        pub value: String,
        /// Type of match which should be returned
        pub value_type: ValueType,
        /// Collection id
        pub collection: String,
        /// More or less information on match
        // TODO: Implement
        pub verbose: bool,
    }

    impl Display for MatchRequest {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            match serde_json::to_string(self) {
                Ok(s) => write!(f, "{s}"),
                Err(_) => write!(f, "{{}}"),
            }
        }
    }

    /// Expected entity types in value
    #[derive(Clone, Deserialize, Serialize)]
    #[serde(rename_all = "lowercase")]
    pub enum ValueType {
        /// A location
        Location,
        /// Person or organisation
        Agent,
    }

    impl Display for ValueType {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            match self {
                ValueType::Location => write!(f, "location"),
                ValueType::Agent => write!(f, "agent"),
            }
        }
    }

    /// A successfully linked entity
    #[derive(Clone, Debug, Serialize)]
    pub struct LinkedEntity {
        /// Value of linked entity string
        pub value: String,
        /// Wikidata's qid (without the `q` prefix)
        pub qid: usize,
        /// Type of match
        pub match_type: MatchType,
        /// Range of match in original value
        pub range: Range<usize>,
        /// Value's languages
        pub lang: Vec<Lang>,
    }

    /// A potential match
    #[derive(Clone, Debug, Serialize)]
    pub struct Match {
        /// The original value
        pub value: String,
        /// List of matches
        pub matches: Vec<LinkedEntity>,
        /// Name of extractor which found the match
        pub extractor: String,
    }

    /// Possible HTTP errors
    pub(super) enum AppError {
        /// No extractors for collection id found
        NoExtractorsFound(String),
        /// No matches found
        NoMatchesFound,
    }

    #[cfg(test)]
    mod tests {
        use super::*;
        use crate::config::ExtractorConfig;

        #[test]
        fn fallback_extractors() {
            let mut agent_configs = HashMap::new();
            agent_configs.insert(
                "abc-001".to_string(),
                vec![
                    ExtractorConfig {
                        name: "noop".to_string(),
                        args: None,
                    },
                    ExtractorConfig {
                        name: "noop".to_string(),
                        args: None,
                    },
                ],
            );
            agent_configs.insert(
                "*".to_string(),
                vec![ExtractorConfig {
                    name: "noop".to_string(),
                    args: None,
                }],
            );
            let extractors = Extractors::build(agent_configs, HashMap::new());
            assert!(extractors.get_agent_extractors("abc-001").len() == 2);
            assert!(extractors.get_agent_extractors("xyz-001").len() == 1);
        }
    }
}

impl From<AppError> for Json<Match> {
    fn from(val: AppError) -> Self {
        match val {
            AppError::NoExtractorsFound(id) => Json(Match {
                value: String::new(),
                matches: vec![],
                extractor: format!("No extractors for collection {id} found"),
            }),
            AppError::NoMatchesFound => Json(Match {
                value: String::new(),
                matches: vec![],
                extractor: "No matches found".to_string(),
            }),
        }
    }
}

/// Assembles API routes
pub fn routes() -> Router<AppState> {
    Router::new()
        .route("/health", get(|| async { StatusCode::OK }))
        .route("/link", post(match_value))
}

/// Handles a request for an location Wikidata QID
fn handle_location_request(state: &AppState, payload: MatchRequest) -> (StatusCode, Json<Match>) {
    let collection = payload.collection;
    let extractors = state.extractors.get_location_extractors(&collection);
    if extractors.is_empty() {
        debug!(
            "No location extractors for collection {} found, therefore returning 404",
            &collection
        );
        return (
            StatusCode::NOT_FOUND,
            AppError::NoExtractorsFound(collection).into(),
        );
    }
    let applicable_extractors = extraction::extract(&payload.value, extractors);

    for (extr, mut found_entities) in applicable_extractors {
        let mut linked_entities: Vec<LinkedEntity> = vec![];
        found_entities.sort();
        let mut state_qid: LocationContext = LocationContext::NotAvailable;
        let mut country_qid: LocationContext = LocationContext::NotAvailable;
        let mut continent_qid: LocationContext = LocationContext::NotAvailable;
        for found_entity in found_entities {
            if let Some(linked_entity) = state.indices.location_lookup(
                found_entity.match_type,
                &found_entity,
                &state_qid,
                &country_qid,
                &continent_qid,
            ) {
                match found_entity.match_type {
                    MatchType::State | MatchType::StateAbbr => {
                        state_qid = LocationContext::Match(linked_entity.qid);
                    }
                    MatchType::Country | MatchType::CountryAbbr => {
                        country_qid = LocationContext::Match(linked_entity.qid);
                    }
                    MatchType::Continent | MatchType::ContinentAbbr => {
                        continent_qid = LocationContext::Match(linked_entity.qid);
                    }
                    _ => {}
                };
                linked_entities.push(linked_entity);
            } else {
                match found_entity.match_type {
                    MatchType::State | MatchType::StateAbbr => {
                        state_qid = LocationContext::UnknownEntity(found_entity.value.to_string());
                    }
                    MatchType::Country | MatchType::CountryAbbr => {
                        country_qid =
                            LocationContext::UnknownEntity(found_entity.value.to_string());
                    }
                    MatchType::Continent | MatchType::ContinentAbbr => {
                        continent_qid =
                            LocationContext::UnknownEntity(found_entity.value.to_string());
                    }
                    _ => {}
                }
            }
        }
        if !linked_entities.is_empty() {
            debug!("Location matches found for request");
            return (
                StatusCode::OK,
                Json(Match {
                    value: payload.value.clone(),
                    matches: linked_entities,
                    extractor: extr.name(),
                }),
            );
        }
    }
    debug!("No location matches found for request");
    (StatusCode::OK, AppError::NoMatchesFound.into())
}

/// Handles a request for an agent Wikidata QID
fn handle_agent_request(state: &AppState, payload: MatchRequest) -> (StatusCode, Json<Match>) {
    let collection = payload.collection;
    let extractors = state.extractors.get_agent_extractors(&collection);
    if extractors.is_empty() {
        debug!(
            "No agent extractors for collection {} found, therefore returning 404",
            &collection
        );
        return (
            StatusCode::NOT_FOUND,
            AppError::NoExtractorsFound(collection).into(),
        );
    }
    let applicable_extractors = extraction::extract(&payload.value, extractors);
    for (extr, found_entities) in applicable_extractors {
        let mut linked_entity: Option<LinkedEntity> = None;
        let mut given_name_entity: Option<FoundEntity> = None;
        let mut family_name_entity: Option<FoundEntity> = None;
        for found_entity in found_entities {
            match found_entity.match_type {
                MatchType::PersonFullName => {
                    linked_entity = state.indices.person_full_name_lookup(&found_entity);
                }
                MatchType::PersonGivenName if family_name_entity.is_some() => {
                    linked_entity = state.indices.person_given_family_name_lookup(
                        &found_entity,
                        &family_name_entity.clone().unwrap(),
                    );
                }
                MatchType::PersonFamilyName if given_name_entity.is_some() => {
                    linked_entity = state.indices.person_given_family_name_lookup(
                        &given_name_entity.clone().unwrap(),
                        &found_entity,
                    );
                }
                MatchType::PersonGivenName => {
                    given_name_entity = Some(found_entity);
                }
                MatchType::PersonFamilyName => {
                    family_name_entity = Some(found_entity);
                }
                _ => {}
            }
        }
        if let Some(le) = linked_entity {
            debug!("Agent matches found for request");
            return (
                StatusCode::OK,
                Json(Match {
                    value: payload.value.clone(),
                    matches: vec![le],
                    extractor: extr.name(),
                }),
            );
        }
    }
    debug!("No matches found for request");
    (StatusCode::OK, AppError::NoMatchesFound.into())
}

/// Handles a match request
async fn match_value(
    State(state): State<AppState>,
    Json(payload): Json<MatchRequest>,
) -> (StatusCode, Json<Match>) {
    debug!("New match request received. Payload: {}", payload);
    match payload.value_type {
        models::ValueType::Location => handle_location_request(&state, payload),
        models::ValueType::Agent => handle_agent_request(&state, payload),
    }
}

#[cfg(test)]
mod tests {
    use std::{collections::HashMap, sync::Arc};

    use models::Extractors;

    use crate::{
        config::{Config, ExtractorConfig},
        indices::{
            locations::{Location, Locations},
            persons::{Person, PersonsIdx},
            Indices,
        },
    };

    use super::*;

    fn indices() -> Indices {
        let glarus_sued = r#"
name: glarus süd
lang: ['de']
qid: 70695
state_qid: 11949
country_qid: 39
continent_qid: 46
        "#;
        let glarus_sued: Location = serde_yaml::from_str(glarus_sued).unwrap();
        let mun = Locations::new(vec![glarus_sued]);

        let gl = r#"
name: gl
lang: [de, fr, it]
qid: 11949
country_qid: 39
continent_qid: 46
        "#;
        let gl: Location = serde_yaml::from_str(gl).unwrap();
        let staa = Locations::new(vec![gl]);

        let ch = r#"
name: schweiz
lang: [de]
qid: 39
continent_qid: 46
        "#;
        let ch: Location = serde_yaml::from_str(ch).unwrap();
        let cou = Locations::new(vec![ch]);

        let eur = r#"
name: europa
lang: [de, it]
qid: 46
        "#;
        let eur: Location = serde_yaml::from_str(eur).unwrap();
        let con = Locations::new(vec![eur]);
        let battenberg = r#"
qid: 160001
labels:
  - name: "victoria eugénie von battenberg"
    lang: ["de"]
  - name: "victoire-eugénie de battenberg"
    lang: ["fr"]
  - name: "vittoria eugenia di battenberg"
    lang: ["it"]
"#;
        let battenberg: Person = serde_yaml::from_str(battenberg).unwrap();
        let per = PersonsIdx::new(vec![battenberg]);

        Indices::builder()
            .mun(mun)
            .sta(Locations::new(vec![]))
            .staa(staa)
            .cou(cou)
            .coua(Locations::new(vec![]))
            .con(con)
            .cona(Locations::new(vec![]))
            .per(per)
            .build()
    }

    fn app_state() -> AppState {
        let config = Arc::new(Config {
            host: "0.0.0.0:3000".to_string(),
            server_cert_path: "/dev/null".to_string(),
            server_key_path: "/dev/null".to_string(),
            ca_cert_path: None,
            cert_reload_interval: 0,
            allowed_client_domains: vec![],
            extractor_configs_path: "/dev/null".to_string(),
            wikidata_mappings_path: "/dev/null".to_string(),
        });
        let only_mun_extractor = ExtractorConfig {
            name: "pattern".to_string(),
            args: Some(r"^(?<mun>.*)$".to_string()),
        };
        let mun_with_ctx_extractor = ExtractorConfig {
            name: "pattern".to_string(),
            args: Some(r"^(?<mun>[-\p{Latin}\. /]+), (?<staa>[A-Z]{2}) \((?<cou>[-\p{Latin}]+)\), (?<con>[\p{Latin} ]+)$".to_string()),
        };
        let loc_extractor_config = vec![mun_with_ctx_extractor, only_mun_extractor];
        let mut location_extractor_configs = HashMap::new();
        location_extractor_configs.insert("abc-001".to_string(), loc_extractor_config);
        let agent_extractor_config = vec![ExtractorConfig {
            name: "pattern".to_string(),
            args: Some("^(?<per>.*)$".to_string()),
        }];
        let mut agent_extractor_configs = HashMap::new();
        agent_extractor_configs.insert("abc-001".to_string(), agent_extractor_config);
        let extractors = Arc::new(Extractors::build(
            agent_extractor_configs,
            location_extractor_configs,
        ));
        let indices = Arc::new(indices());
        AppState {
            config,
            extractors,
            indices,
        }
    }

    #[tokio::test]
    async fn no_extractors() {
        let app_state = State(app_state());
        let match_request = Json(MatchRequest {
            value: "Glarus Süd".to_string(),
            value_type: models::ValueType::Location,
            collection: "cba-001".to_string(),
            verbose: true,
        });
        let res = match_value(app_state, match_request).await;
        assert!(&res.1.extractor == "No extractors for collection cba-001 found");
    }

    #[tokio::test]
    async fn no_match() {
        let app_state = State(app_state());
        let match_request = Json(MatchRequest {
            value: "Glarus Nord".to_string(),
            value_type: models::ValueType::Location,
            collection: "abc-001".to_string(),
            verbose: true,
        });
        let res = match_value(app_state, match_request).await;
        assert!(&res.1.extractor == "No matches found");
    }

    #[tokio::test]
    async fn is_simple_match() {
        let app_state = State(app_state());
        let match_request = Json(MatchRequest {
            value: "Glarus Süd".to_string(),
            value_type: models::ValueType::Location,
            collection: "abc-001".to_string(),
            verbose: true,
        });
        let res = match_value(app_state, match_request).await;
        assert!(&res.1.value == "Glarus Süd");
    }

    #[tokio::test]
    async fn is_partial_match() {
        let app_state = State(app_state());
        let match_request = Json(MatchRequest {
            value: "Glarus Süd, ZH (Schweiz), Europa".to_string(),
            value_type: models::ValueType::Location,
            collection: "abc-001".to_string(),
            verbose: true,
        });
        let res = match_value(app_state, match_request).await;
        // Continent and country should be linked
        assert!(res.1 .0.matches.len() == 2)
    }

    #[tokio::test]
    async fn is_full_match() {
        let app_state = State(app_state());
        let match_request = Json(MatchRequest {
            value: "Glarus Süd, GL (Schweiz), Europa".to_string(),
            value_type: models::ValueType::Location,
            collection: "abc-001".to_string(),
            verbose: true,
        });
        #[allow(clippy::unnecessary_operation, unused_must_use)]
        match_value(app_state, match_request).await;
    }
}
