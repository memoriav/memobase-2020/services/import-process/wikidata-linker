//! Contains utils for handling application configuration

use std::{
    collections::HashMap,
    ffi::OsString,
    fs::{self, File},
    io::{BufReader, Read},
    path::Path,
};

use anyhow::{Context, Result};
use clap::Parser;
use flate2::bufread::GzDecoder;
use serde::Deserialize;
use tracing::info;

use crate::indices::{locations::Location, persons::Person};

/// Extractor configuration model
type ExtractorConfigs = HashMap<String, Vec<ExtractorConfig>>;

/// Possible CLI, envvar and configuration file arguments
#[derive(Deserialize, Default, Parser)]
#[serde(rename_all = "camelCase", deny_unknown_fields)]
#[command(author, version, about, long_about = None)]
#[clap(rename_all = "kebab-case")]
struct InputConfig {
    /// Host address
    #[serde(default)]
    #[arg(short = 'H', long, env, verbatim_doc_comment)]
    pub host: Option<String>,
    /// Path to server certificate file
    #[serde(default)]
    #[arg(long, env, verbatim_doc_comment)]
    pub server_cert_path: Option<String>,
    /// Path to server key file
    #[serde(default)]
    #[arg(long, env, verbatim_doc_comment)]
    pub server_key_path: Option<String>,
    /// Path to CA certificate file
    #[serde(default)]
    #[arg(long, env, verbatim_doc_comment)]
    pub ca_cert_path: Option<String>,
    /// Interval in secs for reloading certificates
    #[serde(default)]
    #[arg(long, env, verbatim_doc_comment)]
    pub cert_reload_interval: Option<u64>,
    /// List of allowed client domains
    #[serde(default)]
    #[arg(long, env, verbatim_doc_comment, value_delimiter = ',')]
    pub allowed_client_domains: Option<Vec<String>>,
    /// Path to record set extractors configuration file
    #[arg(long, env)]
    pub extractor_configs_path: Option<String>,
    /// Path to wikidata mapping files
    #[arg(long, env)]
    pub wikidata_mappings_path: Option<String>,
    /// Path to configuration file
    #[serde(skip_deserializing)]
    #[arg(short = 'c', long, env)]
    pub config_file: Option<String>,
}

/// Merged application configuration
pub struct Config {
    /// Host address
    pub host: String,
    /// Path to server certificate file
    pub server_cert_path: String,
    /// Path to server key file
    pub server_key_path: String,
    /// Path to CA certificate file
    pub ca_cert_path: Option<String>,
    /// Interval in secs for reloading certificates
    pub cert_reload_interval: u64,
    /// List of allowed client domains
    pub allowed_client_domains: Vec<String>,
    /// Path to record set extractors configuration files
    pub extractor_configs_path: String,
    /// Path to wikidata mapping files
    pub wikidata_mappings_path: String,
}

impl Config {
    /// Build application configuration instance by merging settings defined in a configuration
    /// file, in environment variables and on the command line
    pub(self) fn build(cli_config: InputConfig, file_config: InputConfig) -> anyhow::Result<Self> {
        Ok(Self {
            host: cli_config
                .host
                .or(file_config.host)
                .unwrap_or_else(|| "0.0.0.0:3000".to_string()),
            server_cert_path: cli_config
                .server_cert_path
                .or(file_config.server_cert_path)
                .context("setting cert_path required")?,
            server_key_path: cli_config
                .server_key_path
                .or(file_config.server_key_path)
                .context("setting cert_path required")?,
            ca_cert_path: cli_config.ca_cert_path.or(file_config.ca_cert_path),
            cert_reload_interval: cli_config
                .cert_reload_interval
                .or(file_config.cert_reload_interval)
                .unwrap_or(30 * 60),
            allowed_client_domains: cli_config
                .allowed_client_domains
                .or(file_config.allowed_client_domains)
                .context("list of allowed client domains required")?,
            extractor_configs_path: cli_config
                .extractor_configs_path
                .or(file_config.extractor_configs_path)
                .context("path to record set extractors configuration files required")?,
            wikidata_mappings_path: cli_config
                .wikidata_mappings_path
                .or(file_config.wikidata_mappings_path)
                .context("path to wikidata mapping files required")?,
        })
    }
}

/// Creates a [`Config`] instance from the different possible input sources
pub fn read_settings() -> anyhow::Result<Config> {
    let cli_config = InputConfig::parse();
    let path = cli_config
        .config_file
        .clone()
        .unwrap_or_else(|| "configs/main.toml".to_string());
    let file_config = if let Ok(file) = File::open(&path) {
        let mut reader = BufReader::new(file);
        let mut buf = String::new();
        reader.read_to_string(&mut buf)?;
        toml::from_str(&buf)?
    } else {
        info!("No config file found under {}", &path);
        InputConfig::default()
    };
    Config::build(cli_config, file_config)
}

/// Contains information on one extractor
#[allow(clippy::module_name_repetitions)]
#[derive(Deserialize, Debug)]
pub struct ExtractorConfig {
    /// Mandatory name
    pub name: String,
    /// Optional arguments
    pub args: Option<String>,
}

/// Reads extractor settings
pub fn read_extractors_settings<P>(path: P) -> Result<ExtractorConfigs>
where
    P: AsRef<Path>,
{
    let file = fs::read(path)?;
    serde_yaml::from_slice(file.as_ref()).map_err(Into::into)
}

/// Reads a Wikidata location mapping
pub fn read_location_mapping<P>(path: P) -> Result<Vec<Location>>
where
    P: AsRef<Path>,
{
    let file = fs::read(&path)?;
    if path.as_ref().extension().unwrap_or(&OsString::new()) == "gz" {
        let mut gz = GzDecoder::new(&file[..]);
        let mut s = String::new();
        gz.read_to_string(&mut s)?;
        serde_yaml::from_str(&s).map_err(Into::into)
    } else {
        serde_yaml::from_slice(file.as_ref()).map_err(Into::into)
    }
}

/// Reads a Wikidata person mapping
pub fn read_person_mapping<P>(path: P) -> Result<Vec<Person>>
where
    P: AsRef<Path>,
{
    let file = fs::read(&path)?;
    if path.as_ref().extension().unwrap_or(&OsString::new()) == "gz" {
        let mut gz = GzDecoder::new(&file[..]);
        let mut s = String::new();
        gz.read_to_string(&mut s)?;
        serde_yaml::from_str(&s).map_err(Into::into)
    } else {
        serde_yaml::from_slice(file.as_ref()).map_err(Into::into)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_read_extractors_settings() {
        let input = r#"abc-001:
    - name: pattern
      args: a_pattern
    - name: noop"#;
        let yaml: Result<ExtractorConfigs, _> = serde_yaml::from_str(input);
        assert!(yaml.is_ok());
    }
}
