FROM scratch
ENTRYPOINT ["/app/app"]
WORKDIR /app
COPY target/app .
COPY configs/extractors configs/extractors
COPY configs/wd_mappings configs/wd_mappings
