#!/bin/env python3

from datetime import datetime
import json
import sys
from urllib.request import Request, urlopen
from urllib.error import HTTPError
from urllib import parse

def get_labels(obj):
    labels = []
    if "labels" in obj:
        labels = {}
        if "de" in obj["labels"]:
            labels["de"] = obj["labels"]["de"].lower()
        if "fr" in obj["labels"]:
            labels["fr"] =  obj["labels"]["fr"].lower()
        if "it" in obj["labels"]:
            labels["it"] = obj["labels"]["it"].lower()
        if "de" in labels and "fr" in labels and "it" in labels:
            if labels["it"] == labels["fr"] and labels["de"] == labels["it"]:
                labels = [(labels["de"], '["de", "fr", "it"]')]
            elif labels["de"] == labels["fr"]:
                labels = [(labels["de"], '["de", "fr"]'), (labels["it"], '["it"]')]
            elif labels["de"] == labels["it"]:
                labels = [(labels["de"], '["de", "it"]'), (labels["fr"], '["fr"]')]
            elif labels["fr"] == labels["it"]:
                labels = [(labels["de"], '["de"]'), (labels["fr"], '["fr", "it"]')]
            else:
                labels = [(labels["de"], '["de"]'), (labels["fr"], '["fr"]'), (labels["it"], '["it"]')]
        elif "de" in labels and "fr" in labels:
            if labels["de"] == labels["fr"]:
                labels = [(labels["de"], '["de", "fr"]')]
            else:
                labels = [(labels["de"], '["de"]'), (labels["fr"], '["fr"]')]
        elif "de" in labels and "it" in labels:
            if labels["de"] == labels["it"]:
                labels = [(labels["de"], '["de", "it"]')]
            else:
                labels = [(labels["de"], '["de"]'), (labels["it"], '["it"]')]
        elif "fr" in labels and "it" in labels:
            if labels["fr"] == labels["it"]:
                labels = [(labels["fr"], '["fr", "it"]')]
            else:
                labels = [(labels["fr"], '["fr"]'), (labels["it"], '["it"]')]
        elif "de" in labels:
                labels = [(labels["de"], '["de"]')]
        elif "fr" in labels:
                labels = [(labels["fr"], '["fr"]')]
        elif "it" in labels:
                labels = [(labels["it"], '["it"]')]
    return labels
    

def name_lookup(names_obj, cache):
    names = []
    for name_obj in names_obj:
        if "content" in name_obj["value"]:
            id = name_obj["value"]["content"].lower()
            if id not in cache:
                req = Request("https://www.wikidata.org/w/rest.php/wikibase/v0/entities/items/" + id)
                req.add_header("Content-Type", "application/json")
                req.add_header("Authorization", "Bearer " + access_token)
                with urlopen(req) as res:
                    j = json.loads(res.read())
                    labels = get_labels(j)
                    normalised = list(filter(lambda x: x[0], labels))
                    cache[id] = normalised
                    names.append(normalised)
    return names

def get_access_token():
    data = parse.urlencode({"grant_type": "client_credentials", "client_id": client_id, "client_secret": client_secret}).encode()
    req = Request("https://www.wikidata.org/w/rest.php/oauth2/access_token", data=data)
    # req.add_header("Content-Type", "Content-Type": application/x-www-form-urlencoded)
    with urlopen(req) as res:
        j = json.loads(res.read())
        return j["access_token"]


pe = []
for line in sys.stdin:
    pe.append(line.rstrip().replace("http://www.wikidata.org/entity/Q", ""))
from_elem = int(sys.argv[3]) if len(sys.argv) >= 4 else 0
person_entities = pe[from_elem:]

client_id = sys.argv[1]
client_secret = sys.argv[2]

access_token = get_access_token()

given_names = {}
family_names = {}
count = 0

with open("configs/wd_mappings/persons.yaml", "w" if from_elem == 0 else "a") as file_handler:
    for entity in person_entities:
        count += 1
        retry = 0
        while True:
            req = Request("https://www.wikidata.org/w/rest.php/wikibase/v0/entities/items/Q" + entity)
            req.add_header("Content-Type", "application/json")
            req.add_header("Authorization", "Bearer " + access_token)
            try:
                with urlopen(req) as res:
                    j = json.loads(res.read())
                    labels = get_labels(j)
                    if labels:
                        file_handler.write("- qid: " + entity + "\n")
                        file_handler.write("  labels:\n")
                        for label in labels:
                            file_handler.write("    - name: \"" + label[0] + "\"\n")
                            file_handler.write("      lang: \"" + label[1] + "\"\n")
                    else:
                        break
                    if "statements" in j:
                        if "P735" in j["statements"]:
                            given_name = j["statements"]["P735"]
                            labels = name_lookup(given_name, given_names)
                            if labels:
                                file_handler.write("  given_names:\n")
                                for label in labels:
                                    for localised_label in label:
                                        file_handler.write("    - name: \"" + localised_label[0] + "\"\n")
                                        file_handler.write("      lang: \"" + localised_label[1] + "\"\n")
                        if "P734" in j["statements"]:
                            family_name = j["statements"]["P734"]
                            labels = name_lookup(family_name, family_names)
                            if labels:
                                file_handler.write("  family_names:\n")
                                for label in labels:
                                    for localised_label in label:
                                        file_handler.write("    - name: \"" + localised_label[0] + "\"\n")
                                        file_handler.write("      lang: \"" + localised_label[1] + "\"\n")
                        # if "P2561" in j["statements"]:
                            # Monolingual
                            # name = j["statements"]["2561"]
                break
            except HTTPError:
                access_token = get_access_token()
                retry += 1
                if retry > 3:
                    raise Exception("Too many connection retries") 
        if count % 20 == 0:
            
            print(datetime.now().strftime("%y-%m-%sT%H:%M:%S - ") + str(count) + " persons processed")
