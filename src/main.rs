#![allow(clippy::similar_names)]
#![warn(clippy::missing_docs_in_private_items)]

//!  Extracts and links named entities in structured Memobase metadata fields
//!
//! The linker parses structured metadata fields based on a predefined extractor. If
//! an extractor can be applied on a value, the resulting entities (e.g. locations)
//! are checked against a list of Wikidata entities and, if a similar entity is
//! found, enriched with the link to the Wikidata entity. Extractors are defined
//! per collection, and one collection can have multiple extractors. The extractors
//! are applied in the order they are defined in the configuration.

mod api;
mod config;
mod extraction;
mod indices;

use std::{collections::HashMap, path::Path, sync::Arc, time::Duration};

use anyhow::{bail, Context, Result};
use config::{Config, ExtractorConfig};
use tokio::sync::Mutex;
use tracing::{debug, info};
use tracing_subscriber::{layer::SubscriberExt, util::SubscriberInitExt};

use crate::api::models::Extractors;
use indices::{locations::Locations, persons::PersonsIdx, Indices};

#[tokio::main]
async fn main() -> Result<()> {
    tracing_subscriber::registry()
        .with(
            tracing_subscriber::EnvFilter::try_from_default_env()
                .unwrap_or_else(|_| "wikidata_linker=debug".into())
                .add_directive("h2=warn".parse()?)
                .add_directive("hyper=warn".parse()?),
        )
        .with(tracing_subscriber::fmt::layer())
        .init();

    info!("Starting application");
    debug!("Reading application configuration");
    let config = config::read_settings().context("Reading configuration failed")?;

    let location_extractor_configs = read_extractor_config("locations.yaml", "location", &config)?;
    let agent_extractor_configs = read_extractor_config("agents.yaml", "agent", &config)?;

    let extractors = Extractors::build(agent_extractor_configs, location_extractor_configs);

    let indices_builder = Indices::builder();

    debug!("Indexing Wikidata mappings");
    let p = Path::new(&config.wikidata_mappings_path).join("per.yaml.gz");
    debug!(
        "Reading person mapping from {}",
        &p.to_str().unwrap_or("<invalid path>")
    );
    let per_mapping = config::read_person_mapping(&p).context("Reading person mapping failed")?;

    let indices = indices_builder
        .mun(read_location_mapping("mun.yaml", "municipality", &config)?)
        .sta(read_location_mapping("sta.yaml", "state", &config)?)
        .staa(read_location_mapping(
            "staa.yaml",
            "state abbrevation",
            &config,
        )?)
        .cou(read_location_mapping("cou.yaml", "country", &config)?)
        .coua(read_location_mapping(
            "coua.yaml",
            "country abbrevation",
            &config,
        )?)
        .con(read_location_mapping("con.yaml", "continent", &config)?)
        .cona(read_location_mapping(
            "cona.yaml",
            "continent abbrevation",
            &config,
        )?)
        .per(PersonsIdx::new(per_mapping))
        .build();

    info!("Starting webserver on {}", config.host);

    let mtls_config = Arc::new(Mutex::new(
        tokio_mtls_utils::mtls::Config::new(
            &config.server_key_path,
            &config.server_cert_path,
            config.ca_cert_path.as_deref(),
            &config
                .allowed_client_domains
                .iter()
                .map(String::as_str)
                .collect::<Vec<&str>>(),
        )
        .expect("mTLS setup failed"),
    ));

    let cert_reload_task = tokio_mtls_utils::renew_certificates(
        mtls_config.clone(),
        Duration::from_secs(config.cert_reload_interval),
    )
    .await
    .expect("Certificate reloading failed");

    let host = config.host.clone();

    let app_state = api::models::AppState {
        config: Arc::new(config),
        extractors: Arc::new(extractors),
        indices: Arc::new(indices),
    };

    let routes = api::routes().with_state(app_state);

    let webserver_task =
        tokio::spawn(async move { tokio_mtls_utils::serve(routes, mtls_config, host).await });
    match tokio::try_join!(cert_reload_task, webserver_task) {
        Ok(_) => {
            info!("Shutting down application as requested");
            Ok(())
        }
        Err(e) => bail!("An error happened: {e}. Shutting down application"),
    }
}

/// Reads an extractor configuration
fn read_extractor_config(
    file_name: &str,
    extractor_name: &str,
    config: &Config,
) -> Result<HashMap<String, Vec<ExtractorConfig>>> {
    let p = Path::new(&config.extractor_configs_path).join(file_name);
    let p_str = &p.to_str().unwrap_or("<invalid path>");
    debug!(
        "Reading {} extractors configuration from {}",
        extractor_name, p_str
    );
    config::read_extractors_settings(&p).with_context(|| {
        format!("Reading record set {extractor_name} extractors configuration from {p_str} failed")
    })
}

/// Reads a location mapping and logs additional information
fn read_location_mapping(
    file_name: &str,
    mapping_name: &str,
    config: &Config,
) -> Result<Locations> {
    let p = Path::new(&config.wikidata_mappings_path).join(file_name);
    let p_str = &p.to_str().unwrap_or("<invalid path>");
    debug!("Reading {} mapping from {}", mapping_name, p_str);
    Ok(Locations::new(
        config::read_location_mapping(&p)
            .with_context(|| format!("Reading {mapping_name} mapping from {p_str} failed"))?,
    ))
}
