//! Contains the utils to index location entities from Wikidata

use std::{
    collections::{HashMap, HashSet},
    hash::BuildHasher,
};

use cityhasher::CityHasher;
use serde::Deserialize;

use super::Lang;
use crate::{api::models::LinkedEntity, extraction::FoundEntity};

#[derive(PartialEq, Eq)]
/// Contains the reference to a Wikidata entity, if location found and identified
pub enum LocationContext {
    /// No location found
    NotAvailable,
    /// Location found, but no match in Wikidata
    UnknownEntity(String),
    /// A match with respective Wikidata QID
    Match(usize),
}

impl LocationContext {
    /// No location found
    pub fn not_available(&self) -> bool {
        *self == LocationContext::NotAvailable
    }

    /// Return Wikidata QID
    pub fn get_qid(&self) -> Option<usize> {
        match self {
            LocationContext::NotAvailable | LocationContext::UnknownEntity(_) => None,
            LocationContext::Match(qid) => Some(*qid),
        }
    }
}

/// Wikidata municipality entity
#[derive(Clone, Debug, Deserialize)]
pub struct Location {
    /// Municipality name (main, alternative, abbreviated, language-specific name...)
    name: String,
    /// Related languages
    lang: Vec<Lang>,
    /// Municipality's Wikidata ID (without the Q prefix)
    qid: usize,
    /// State's Wikidata ID (without the Q prefix)
    #[serde(skip_serializing_if = "Option::is_none")]
    state_qid: Option<usize>,
    /// Country's Wikidata ID (without the Q prefix)
    #[serde(skip_serializing_if = "Option::is_none")]
    country_qid: Option<usize>,
    /// Continent's Wikidata ID (without the Q prefix)
    #[serde(skip_serializing_if = "Option::is_none")]
    continent_qid: Option<usize>,
}

/// A [`Location`] struct without the [`Location::name`] field for a smaller index
#[derive(Clone, Debug, PartialEq, Eq, Hash)]
pub struct WikidataLinks {
    /// Related languages
    lang: Vec<Lang>,
    /// Municipality's Wikidata ID (without the Q prefix)
    qid: usize,
    /// State's Wikidata ID (without the Q prefix)
    state_qid: Option<usize>,
    /// Country's Wikidata ID (without the Q prefix)
    country_qid: Option<usize>,
    /// Continent's Wikidata ID (without the Q prefix)
    continent_qid: Option<usize>,
}

impl From<Location> for WikidataLinks {
    fn from(value: Location) -> Self {
        Self {
            lang: value.lang,
            qid: value.qid,
            state_qid: value.state_qid,
            country_qid: value.country_qid,
            continent_qid: value.continent_qid,
        }
    }
}

/// Contains the index to look up Wikidata location entities
#[derive(Clone, Debug, PartialEq, Eq)]
pub struct Locations {
    /// Mapping from hashed Wikidata links to [`WikidataLinks`] instances
    table: HashMap<u64, WikidataLinks>,
    /// Mapping from location labels to hashed [`WikidataLinks`] instances
    index: HashMap<String, HashSet<u64>>,
}

impl Locations {
    /// Creates location indices
    pub fn new(locations: Vec<Location>) -> Self {
        let mut index: HashMap<String, HashSet<u64>> = HashMap::with_capacity(locations.len());
        let mut table = HashMap::with_capacity(locations.len());
        let hasher = CityHasher::with_seed(1);
        for location in locations {
            let label = location.name.clone();
            let wikidata_links: WikidataLinks = location.into();
            let id = hasher.hash_one(&wikidata_links);
            index
                .entry(label)
                .and_modify(|entries| {
                    entries.insert(id);
                })
                .or_insert_with(|| {
                    let mut hs = HashSet::new();
                    hs.insert(id);
                    hs
                });
            table.entry(id).or_insert(wikidata_links);
        }
        Self { table, index }
    }

    /// Searches Wikidata location entities with matching municipality, state, country and
    /// continent information
    pub fn lookup(
        &self,
        entity: &FoundEntity,
        state_qid: &LocationContext,
        country_qid: &LocationContext,
        continent_qid: &LocationContext,
    ) -> Option<LinkedEntity> {
        if let Some(ids) = self.index.get(&entity.value.to_lowercase()) {
            for id in ids {
                if let Some(wd_link) = self.table.get(id) {
                    if (continent_qid.not_available()
                        || continent_qid.get_qid() == wd_link.continent_qid)
                        && (country_qid.not_available()
                            || country_qid.get_qid() == wd_link.country_qid)
                        && (state_qid.not_available() || state_qid.get_qid() == wd_link.state_qid)
                    {
                        return Some(LinkedEntity {
                            value: entity.value.to_string(),
                            //qid: format!("http://www.wikidata.org/entity/Q{}", wd_link.qid),
                            qid: wd_link.qid,
                            match_type: entity.match_type,
                            range: entity.range.clone(),
                            lang: wd_link.lang.clone(),
                        });
                    }
                }
            }
        }
        None
    }
}
